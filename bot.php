<?php

/**
 * Php starter kit
 * @see https://www.hackerearth.com/battle-of-bots-5/multiplayer/reversi/game/575454/
 *
 * @author davidovich.denis@gmail.com
 */

class Board
{
    const GRID_DIMENSION = 10;

    const CELL_STATUS_EMPTY = 0;
    const CELL_STATUS_PLAYER1 = 1;
    const CELL_STATUS_PLAYER2 = 2;
    const CELL_STATUS_ALLOWED = 3;
}

class Reader
{
    /**
     * Read current step state of game
     */
    public function getState() {
        $board  = $this->getBoard();
        $player = $this->getPlayer();

        if (defined('DEBUG_MODE')) {
            $this->showBoard($board);
            echo $player . "\n";
        }

        return [
            $board,
            $player,
        ];
    }

    /**
     * Read state of board
     *
     * @return array
     */
    function getBoard() {
        $aBoard = [];
        $n = Board::GRID_DIMENSION;
        while ($n--) {
            $sRow = trim(fgets(STDIN));
            $aBoard[] = explode(' ', $sRow);
        }

        return $aBoard;
    }

    /**
     * Get current player Id
     *
     * @return mixed
     */
    function getPlayer() {
        fscanf(STDIN, "%i", $iPlayer);
        return $iPlayer;
    }

    /**
     * Print current state of board
     *
     * @param $aBoard
     */
    function showBoard($aBoard) {
        foreach ($aBoard as $aRow) {
            echo implode(' ', $aRow), "\n";
        }
        echo "\n";
    }
}

class Game
{
    static public function run() {
        $reader = new Reader();
        $strategy = new Strategy();

        $aMove = $strategy->move($reader->getState());

        echo implode(' ', $aMove), "\n";
    }
}

class Strategy
{
    /**
     * Current state of board
     *
     * @var array
     */
    public $board = [];

    /**
     * Id Current Player
     *
     * @var int
     */
    public $player = 0;

    /**
     * Make new move.
     *
     * @param $aState [$aBoard, $iPlayer]
     *
     * @return array
     */
    public function move($aState) {

        list($this->board, $this->player) = $aState;

        $aSteps = $this->_getPossibleSteps($this->board);
        $aMove = $this->_getRandomStep($aSteps);

        return $aMove;
    }

    protected function _getPossibleSteps($aBoard) {
        $aSteps = [];
        foreach ($aBoard as $i => $aRow) {
            foreach ($aRow as $j => $iStatus)
                if ($iStatus == Board::CELL_STATUS_ALLOWED) {
                    $aSteps[] = [$i, $j];
                }
        }
        return $aSteps;
    }

    /**
     * Get random step
     *
     * @param $aSteps
     * @return array
     */
    protected function _getRandomStep($aSteps) {
        $iStep = rand(0, count($aSteps) - 1);
        return $aSteps[$iStep];
    }
}

//define('DEBUG_MODE', 1);
Game::run();